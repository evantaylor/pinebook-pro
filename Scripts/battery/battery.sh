#!/bin/bash
# Script checks the battery charge/discharge levels, useful for testing workloads and power supplies.
# Modified from Der Geist der Maschine's script found on his review and then completely reworked by Unicorn because apparently bash > sh and something something readability.
# see http://students.engr.scu.edu/~sschaeck/misc/pinebookpro.html

for file in /usr/lib/bash/sleep /usr/lib32/bash/sleep /usr/lib64/bash/sleep; do [ -r "$file" ] && enable -f "$file" sleep && break; done

printf "%s\t%s\t%s\t%s\n" Timestamp % Current Voltage

while true; do 
    printf -v dt "%(%H:%M:%S)T"
    read capacity < /sys/class/power_supply/cw2015-battery/capacity
    read current < /sys/class/power_supply/cw2015-battery/current_now
    [ $current -eq 0 ] && { [ $capacity -lt 100 ] && current="CHRG" || current="FULL"; }
    read voltage < /sys/class/power_supply/cw2015-battery/voltage_now
    echo -e "$dt\t$capacity\t$current\t$voltage"
    sleep 5
done | tee battery.txt
