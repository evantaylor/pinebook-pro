#!/bin/sh
# Script checks the battery charge/discharge levels, useful for testing workloads and power supplies.
# Modified from Der Geist der Maschine's script found on his review.
# see http://students.engr.scu.edu/~sschaeck/misc/pinebookpro.html
while true; do date +%H:%M:%S | tr "\n" "\t"; cat /sys/class/power_supply/cw2015-battery/capacity | tr "\n" "\t"; cat /sys/class/power_supply/cw2015-battery/current_now | tr "\n" "\t"; cat /sys/class/power_supply/cw2015-battery/voltage_now; sleep 1; done | tee battery.txt
