sudo systemd-analyze blame

  2min 143ms systemd-networkd-wait-online.service             
1min 33.845s man-db.service                                   
     13.096s updatedb.service                                 
      4.223s NetworkManager-wait-online.service               
      2.508s lvm2-monitor.service                             
      1.999s dev-mmcblk2p1.device                             
      1.313s tlp.service                                      
      1.036s bootsplash-hide-when-booted.service              
      1.009s systemd-logind.service                           
       988ms systemd-hwdb-update.service                      
       944ms systemd-tmpfiles-clean.service                   
       779ms systemd-random-seed.service                      
       595ms upower.service                                   
       559ms ldconfig.service                                 
       512ms zram-swap.service                                
       506ms accounts-daemon.service                          
       469ms lightdm.service                                  
       416ms avahi-daemon.service                             
       406ms polkit.service                                   
       381ms systemd-udev-trigger.service                     
       359ms smb.service                                      
       339ms systemd-udevd.service  

man-db.service takes incredibly long to startup and isn't critical.

sudo systemctl.disable man-db.service
