# Pinebook-Pro

Pinebook Pro related scripts, configurations, and walk-through guides.

# Fixing the trackpad

The default trackpad is nearly unusable even after applying the firmware fix.

Change to the synaptics input driver and most of the remaining problems go away.

sudo pacman -Sy xf86-input-synaptics



# Libreoffice-fresh spellchecking
Libreoffice for Manjaro ARM requires installing the hunspell package to make spellcheck and automatic spellchecking function.

To find your locale's hunspell package:

sudo pacman -Ss hunspell

Or just install the en_US version (like I did):

sudo pacman -Sy hunspell-en_US
